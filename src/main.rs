use std::alloc::System;

// to reduce binary size
#[global_allocator]
static A: System = System;

use hsl::HSL;
use std::{
    io::{stdin, Write},
    process::exit,
};
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

fn main() {
    let mut stdout = StandardStream::stdout(ColorChoice::Always);
    let mut buffer = String::new();
    let stdin = stdin();

    let mut line_hue = 0;
    let line_hue_step = 10;
    let per_char_step = 9;

    loop {
        match stdin.read_line(&mut buffer) {
            Ok(0) => exit(0),
            Ok(_) => {
                print_colored(line_hue, &buffer, &mut stdout, per_char_step);
                line_hue += line_hue_step;
                buffer.clear();
            }
            Err(e) => {
                println!("{e}");
                exit(1)
            }
        }
    }
}

fn print_colored(mut hue: u32, buffer: &str, stdout: &mut StandardStream, per_char_step: u32) {
    for char in buffer.chars() {
        hue = (hue + per_char_step) % 360;
        let color = HSL {
            h: hue as f64,
            s: 1.0,
            l: 0.5,
        }
        .to_rgb();
        stdout
            .set_color(ColorSpec::new().set_fg(Some(Color::Rgb(color.0, color.1, color.2))))
            .unwrap();

        write!(stdout, "{char}").unwrap();
    }
}
